import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {from, Observable} from 'rxjs';
import {DoctorsService} from '../../services/doctors.service'
@Component({
  selector: 'app-doctors-appointment',
  templateUrl: './doctors-appointment.component.html',
  styleUrls: ['./doctors-appointment.component.css']
})
export class DoctorsAppointmentComponent implements OnInit {

  details:any[]=[]
  constructor(private service : DoctorsService, private http: HttpClient) { }

  ngOnInit(): void {
    this.service.getData().subscribe(data=>{
      this.details=data;
    })
  }


  onDelete(phoneNumber:any) {
    console.log(phoneNumber);

   this.service.deleteContact(phoneNumber).subscribe(data=>{
     window.location.reload();
     console.log(this.details);


   })
  }



}
