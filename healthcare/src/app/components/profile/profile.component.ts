import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { OtpAuthService } from 'src/app/services/otp-auth.service';
import { AuthService } from '../../services/auth.service';
import {MyhealthService} from '../../services/myhealth.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input() myHealth
  user: any
  health:any
  addMyHealthData:Object[]=[]
  id:Number
  healths:any
  userId:Number
  otpUser:any
  myphoneNumber:any
  @Input() data

myId:any
  constructor(private authService: AuthService,
    private myhealthService:MyhealthService,
    private route:ActivatedRoute,
    private otpService:OtpAuthService
    ) { }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      // console.log(JSON.stringify(profile));
      
      this.user = (profile as any).user;
      // console.log(this.user);
      // console.log('User Details ',this.user);
      
      
      
     })

    



    this.myhealthService.getHealth().subscribe(data => {
      // console.log(data);
      // this.addMyHealthData.push(data)

      this.health = (data as any)
      // console.log((data as any).health);
      // console.log('my health', this.addMyHealthData);
      
      
      this.myhealthService.getHealthByID(this.user._id).subscribe(data => {
        this.healths = (data as any).health
        // console.log(this.healths);
        
      })

    })

this.otpService.sendphoneNumber(this.data).subscribe(data=>{
  console.log(data);
  
  this.otpUser=data

  
})



     
  }
}