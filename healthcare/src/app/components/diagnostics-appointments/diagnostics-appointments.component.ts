import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { DiagnosticsService } from '../../services/diagnostics.service';

@Component({
  selector: 'app-diagnostics-appointments',
  templateUrl: './diagnostics-appointments.component.html',
  styleUrls: ['./diagnostics-appointments.component.css']
})
export class DiagnosticsAppointmentsComponent implements OnInit {
  details: any[] = []
  constructor(private service: DiagnosticsService, private http: HttpClient) { }

  ngOnInit(): void {
    this.service.getData().subscribe(data => {
      this.details = data;
    })
  }

  // onDelete(id:any){
  //   console.log(id);
  //   this.service.deleteContact(id).subscribe(data=>{
  //     this.service.getData()

  //   })
  // baseUrl='http://localhost:9001/diagnostics'
  onDelete(name: any) {
    console.log(name);

    this.service.deleteContact(name).subscribe(data => {
      window.location.reload();
      console.log(this.details);


    })
  }

}

