import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { projectService} from '../../services/project.service';
@Component({
  selector: 'caloriemeter',
  templateUrl: './caloriemeter.component.html',
  styleUrls: ['./caloriemeter.component.css']
})
export class CaloriemeterComponent implements OnInit {
   metobolicrate:any
  name= new FormControl('',[Validators.required])
  age:any= new FormControl('',[Validators.required, Validators.pattern('^\\d{2}$')])
  gender :any= new FormControl('')
   weight:any=new FormControl('',[Validators.required,/* Validators.pattern('^\\d{2}$')*/])
   height :any=new FormControl('',[Validators.required,/* Validators.pattern('/^[0-9]+(\.[0-9]{1,2})?$/')*/])
   calorieForm: FormGroup
   genders=['male','female']
   mobileNumber= new FormControl('', [Validators.required, Validators.pattern('^\\d{10}$')])
 
   detailsArray:any[]=[];
   details:any;
  
  
  constructor(private fb : FormBuilder, private service : projectService) {
      this.calorieForm= this.fb.group({
        name: this.name,
        age   : this.age,
      gender: this.gender,
       weight:this.weight,
       height:this.height,
      mobileNumber :this.mobileNumber,
    })
    }
    
    onRegister(){
      console.log(this.calorieForm);
      alert('Submitted');
    }
    
    ngOnInit(): void {
      // this.service.getData().subscribe(userdata => this.detailsArray=userdata)
    }
    
    addContact(){
      const newData={
        name: this.calorieForm.value.name,
        age: this.calorieForm.value.age,
        gender: this.calorieForm.value.gender,
        weight:this.calorieForm.value.weight,
        height:this.calorieForm.value.height,
        checkmetobolicrate:this.calorieForm.value.checkmetobolicrate,
        mobileNumber: this.calorieForm.value.mobileNumber
      }
      this.service.addContact(newData).subscribe(Contact=>{
        this.detailsArray.push(Contact)
      })
    }
    calculatemetobolicrate() {
      console.log(this.calorieForm.value.gender);
      
      if (this.calorieForm.value.gender=='male')
      {
        this.calorieForm.value.metobolicrate = 10 * this.calorieForm.value.weight + 6.25 * this.calorieForm.value.height - 5 * this.calorieForm.value.age + 5;
        this.calorieForm.value.metobolicrate = Math.round(this.calorieForm.value.metobolicrate);
        console.log(this.calorieForm.value.metobolicrate);
        
      }
      else 
      {
        this.calorieForm.value.metobolicrate = 10 * this.calorieForm.value.weight + 6.25 * this.calorieForm.value.height - 5 * this.calorieForm.value.age - 161;
      this.calorieForm.value.metobolicrate = Math.round(this.calorieForm.value.metobolicrate);
      console.log(this.calorieForm.value.metobolicrate);
      } 
    }
    
    

}

