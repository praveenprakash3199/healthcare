import { OtpAuthService } from '../../services/otp-auth.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
//import { WindowService } from './../window.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
//import firebase from 'c:/ProgramData/Microsoft/Windows/Start Menu/Programs/Node.js/Angular/angular-register/src/firebase.js';
//import * as firebase from 'firebase';
@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OTPComponent implements OnInit {

  phoneNumber: any;
  OTPForm: FormGroup;
  VerifyForm: FormGroup;
  verifyNumber: any;
  info;
 data
  constructor(private fb: FormBuilder, private otpService: OtpAuthService, private _router: Router
    ,private route:ActivatedRoute
    ) {
    this.createOTPForm();
    this.createVerifyForm();
  }
  phoneNo:any;
  ngOnInit(): void {
    // this.route.paramMap.subscribe(params=>{
    //   this.phoneNo=params.get('phoneNumber')
    //   console.log('from otp component OnInit',this.phoneNo);
     //  this.profileImage=params.get('profileImage')
     //  console.log('my Image',this.profileImage);

   // })

  }
  createOTPForm() {
    this.OTPForm = this.fb.group({
      phoneNumber: new FormControl('', [Validators.required])

    })
  }
  createVerifyForm() {
    this.VerifyForm = this.fb.group({
      verifyNumber: new FormControl('', [Validators.required])
    })
  }
  sendLoginCode() {
    console.log(this.phoneNumber);

    this.otpService.getOTPCode(this.phoneNumber).subscribe(data => {
      console.log(data);
    })
  }

  verifyLoginCode() {
    this.otpService.checkCode(this.phoneNumber, this.verifyNumber).subscribe(data => {
      console.log(data);
      localStorage.setItem('token', data.token);
      // alert("Successfully login with otp")
      console.log(this.phoneNumber);
    // window.localStorage.setItem('userInformation',JSON.stringify(data))
    //   this.info=window.localStorage.getItem('userInformation')
    //   console.log("from user-information component",this.info)

      this.otpService.sendphoneNumber(this.phoneNumber).subscribe(response => {
        console.log(response);
        window.localStorage.setItem('userInformation',JSON.stringify(response))
      this.info=window.localStorage.getItem('userInformation')
      console.log("from user-information component",this.info)
        if (response === null) {
          this._router.navigate(['/userInfo']);
        }
        else {
          this._router.navigate(['/dashboard']);
        }
      })

    },
      err => {
        if (err.status === 401) {
          console.log(err);
          this._router.navigate(['otp']);
        }
      })
  }
}
