import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicineFeverComponent } from './medicine-fever.component';

describe('MedicineFeverComponent', () => {
  let component: MedicineFeverComponent;
  let fixture: ComponentFixture<MedicineFeverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicineFeverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicineFeverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
