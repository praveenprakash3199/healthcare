import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { FlashMessage } from 'angular2-flash-messages/module/flash-message';
import { AdminLoginService } from 'src/app/services/admin-login.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
adminId:any

password:any
  constructor(
public adminService:AdminLoginService,
    private router: Router,
    private flashMessage: FlashMessagesService,

  ) { }

  ngOnInit(): void {
  }
  onLoginSubmit(){
    console.log(this.adminId);
    const user = {
    adminId: this.adminId,
      password: this.password,

    }
    console.log(user);



    this.adminService.authenticateUser(user).subscribe(data => {
      if ((data as any).success) {
        this.adminService.storeUserData((data as any).token, (data as any).user);
        this.flashMessage.show('You are now logged in', {
          cssClass: 'alert-success',
          timeout: 1000
        });
        this.router.navigate(['admin']);
      } else {
        this.flashMessage.show((data as any).msg, {
          cssClass: 'alert-danger',
          timeout: 1000
        });
        this.router.navigate(['admin-login']);
      }
    });
  }

}


  


