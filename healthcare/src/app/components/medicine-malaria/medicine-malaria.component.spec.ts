import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicineMalariaComponent } from './medicine-malaria.component';

describe('MedicineMalariaComponent', () => {
  let component: MedicineMalariaComponent;
  let fixture: ComponentFixture<MedicineMalariaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicineMalariaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicineMalariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
