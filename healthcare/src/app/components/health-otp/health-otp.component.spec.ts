import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthOtpComponent } from './health-otp.component';

describe('HealthOtpComponent', () => {
  let component: HealthOtpComponent;
  let fixture: ComponentFixture<HealthOtpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthOtpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
