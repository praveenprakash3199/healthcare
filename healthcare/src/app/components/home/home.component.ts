import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactusService } from '../../services/contactus.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
name:any
phone:any
email:any
message:any
  phoneNumberValidation = /^(\+\d{1,3}[- ]?)?\d{10}$/;
contactform:FormGroup
  constructor(private fb:FormBuilder,

    private contactService:ContactusService) {
this.contactUsform()

   }

  ngOnInit(): void {
  }
contactUsform(){

this.contactform=this.fb.group({
  name:new FormControl('',[Validators.required]),
  phone: new FormControl('', [Validators.required]),
  email: new FormControl('', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$/)]),
message: new FormControl('', [Validators.required]),
})
}

onSubmit(){
console.log(this.name);
console.log(this.phone);
const contact={
  name:this.name,
  phone:this.phone,
  email:this.email,
  message:this.message
}
this.contactService.contact(contact).subscribe(data=>{
  console.log(data);

})


}

}
