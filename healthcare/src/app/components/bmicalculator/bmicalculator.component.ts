import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CaloriemeterComponent } from '../caloriemeter/caloriemeter.component';
import { BmiService } from '../../services/bmi.service';

@Component({
  selector: 'bmicalculator',
  templateUrl: './bmicalculator.component.html',
  styleUrls: ['./bmicalculator.component.css']
})
export class BmicalculatorComponent implements OnInit {
  bmi:any
  name=new FormControl('',Validators.required)
  height :any=new FormControl('',[Validators.required, Validators.pattern('^\\d{1}$')])
  weight : any=new FormControl('',[Validators.required, Validators.pattern('^\\d{2}$')])
  bmicheck:any=new FormControl('',[Validators.required])
  bmiForm: FormGroup
  detailsArray:any[]=[];
  details:any;

  constructor(private fb:FormBuilder,private service:BmiService,private router:Router) { 
    this.bmiForm=this.fb.group({
      name:this.name,
      height:this.height,
      weight:this.weight,
       bmicheck:this.bmicheck
    })
  }
  onRegister(){
    console.log(this.bmiForm);
    alert("summitted");
    }
    ngOnInit(): void {
      this.service.getData().subscribe(userdata =>this.detailsArray=userdata)
    }
  
    addContact1(){
      const newData={
        name:this.bmiForm.value.name,
        height:this.bmiForm.value.height,
        weight:this.bmiForm.value.weight,
         bmicheck:this.bmiForm.value.bmicheck
      }
      
      this.service.addContact1(newData).subscribe(contact=>{
        this.detailsArray.push(contact)
      })
  }
  calculatebmi(){
    this.bmiForm.value.height=this.bmiForm.value.height*12*0.025
    this.bmi=Math.round(this.bmiForm.value.weight/(this.bmiForm.value.height*this.bmiForm.value.height))
    console.log(this.bmiForm.value.height);
    console.log(this.bmiForm.value.weight);
    
    
    console.log(this.bmi);
    
  }
}
