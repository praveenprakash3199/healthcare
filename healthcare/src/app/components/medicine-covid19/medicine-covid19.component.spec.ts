import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicineCovid19Component } from './medicine-covid19.component';

describe('MedicineCovid19Component', () => {
  let component: MedicineCovid19Component;
  let fixture: ComponentFixture<MedicineCovid19Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicineCovid19Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicineCovid19Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
