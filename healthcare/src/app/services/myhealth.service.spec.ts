import { TestBed } from '@angular/core/testing';

import { MyhealthService } from './myhealth.service';

describe('MyhealthService', () => {
  let service: MyhealthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyhealthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
