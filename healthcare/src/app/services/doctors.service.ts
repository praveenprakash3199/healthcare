import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DoctorsService {
  private url ='http://localhost:9001/Doctors'
  constructor(private http : HttpClient) { }

  getData(){
    return this.http.get<any[]>('http://localhost:9001/Doctors')
  }

  addContact(newContact:any){
    return this.http.post<any>('http://localhost:9001/Doctors',newContact)
  }

  deleteContact(phoneNumber:any): Observable<any>{

    return this.http.delete(this.url+`/${phoneNumber}`);
  }

  getTest(value:any):Observable<any[]>{
    // console.log(this.url+`/${value}`);

    return this.http.get<any[]>(this.url+`/${value}`)
  }



}

