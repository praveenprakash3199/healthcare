import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiagnosticsService {
  private url = 'http://localhost:9001/diagnostics'
  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get<any[]>('http://localhost:9001/diagnostics')
  }

  addContact(newContact: any) {
    return this.http.post<any>('http://localhost:9001/diagnostics', newContact)
  }

  deleteContact(id: any): Observable<any> {

    return this.http.delete(this.url + `/${id}`);
  }

  getTest(value: any): Observable<any[]> {
    // console.log(this.url+`/${value}`);

    return this.http.get<any[]>(this.url + `/${value}`)
  }




}
