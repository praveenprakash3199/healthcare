import { HttpClient, HttpHeaders } from '@angular/common/http'
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators'
// import { tokenNotExpired } from 'angular2-jwt';
import { JwtHelperService } from "@auth0/angular-jwt";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminLoginService {
  authToken: any;
  user: any;
  myId: any;
  constructor(private http:HttpClient) { }
  registerUser(user) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:9001/admins/registers', user, { headers: headers })
      .pipe(map(res => res))
  }
  authenticateUser(user) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:9001/admins/authenticate', user, { headers: headers })
      .pipe(map(res => res))



  }
  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
    this.myId = user.id


  }
  getProfile() {
    let headers = new HttpHeaders();
    this.loadToken();
    // headers.append('Authorization', this.authToken);
    // headers.append('Content-Type', 'application/json');
    // headers = headers.append('Authorization', this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:9001/admins/profiles', { headers })
      .pipe(map(res => res
      ))


  }
  loadToken() {
    const token = localStorage.getItem('id_token');
    // console.log(token);

    this.authToken = token;
  }
  loggedIn() {

    if (localStorage.id_token == undefined) {
      return false
    } else {

      const helper = new JwtHelperService();
      // console.log(!helper.isTokenExpired(localStorage.id_token));
      return !helper.isTokenExpired(localStorage.id_token);
    }
  }
  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

}
