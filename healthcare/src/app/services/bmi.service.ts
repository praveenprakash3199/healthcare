import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class BmiService {

  constructor(private httpclient : HttpClient) { }

  getData(){
    return this.httpclient.get<any[]>('http://localhost:9001/bmi')
  }

  addContact1(newContact: any){
    return this.httpclient.post<any>('http://localhost:9001/bmi',newContact)
  }
}

