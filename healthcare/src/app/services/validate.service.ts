import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {
  constructor() {}

  validateRegister(user) {
    if (
      user.email == undefined ||
      user.username == undefined ||
      user.password == undefined ||
      user.phone == undefined ||
      user.Address == undefined ||
      user.cpassword === undefined
    ) {
      return false;
    } else {
      return true;
    }
  }

  validateEmail(email) {
    const reg =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(email);
  }
  validatePassword(password) {
    if (password.length < 6) {
      return false;
    } else {
      return true;
    }
  }
  validateConfirmPassword(password, cpassword) {
    if (password.length !== cpassword.length) {
      console.log('from if', cpassword);
      return false;
    } else if (cpassword !== password) {
      console.log('form else if ', cpassword);

      return false;
    } else {
      console.log('from else on;y', cpassword);
      return true;
    }
  }
  validatePhone(phone) {
    const reg = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
    return reg.test(phone);
  }

  validateHealthInfo(healthInfo) {
    if (
      healthInfo.bmi == undefined ||
      healthInfo.age == undefined ||
      healthInfo.height == undefined ||
      healthInfo.weight == undefined ||
      healthInfo.medInsurance == undefined ||
      healthInfo.sleepPerDay == undefined ||
      healthInfo.bloodgp == undefined ||
      healthInfo.maritalst == undefined
    ) {
      return false;
    } else {
      return true;
    }
  }
}
