const mongoose = require("mongoose");
const todoSchema=  mongoose.Schema({
    name : {
        type : String,
        required : true,
    },
      age :{
         type: Number,
         required : true,
     },
     gender :{
        type: String,
        required : true,
    },
    test :{
       type:String,
       required : true,
   },
   date :{
     type: String,
     required:true,
   },
   time :{
     type:String,
     required:true,
   },
   mobileNumber:{
     type:Number,
     required:true
   }

})

const TodoModel = mongoose.model("diagnostics",todoSchema);
module.exports={TodoModel};
