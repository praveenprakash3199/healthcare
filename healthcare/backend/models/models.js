const mongoose = require('mongoose');
const DoctorSchema = mongoose.Schema({
  
    first_name : {
        type : String,
        required : true,
    },
    last_name : {
        type : String,
        required : true,
    },
      age :{
         type: Number,
         required : true,
     },
     gender :{
        type: String,
        required : true,
    },
    test :{
       type:String,
       required : true,
   },
   date :{
     type: String,
     required:true,
   },
   time :{
     type:String,
     required:true,
   },
   phoneNumber:{
     type:Number,
     required:true
   }

});
const Doctor = mongoose.model('doctors',DoctorSchema);
//const Doctor = module.exports = mongoose.model('Doctor',DoctorSchema);
module.exports = { Doctor};
