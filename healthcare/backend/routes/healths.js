const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config=require('../config/db')
const multer=require('multer')
const path=require('path')


const storage=multer.diskStorage({
  destination:function(req,file,cb){
    cb(null,'./uploads/')
  },
  filename:function(req,file,cb){
    const ext=file.mimetype.split('/')[1]
    cb(null,file.fieldname,+"_"+Date.now+"."+ext)
  }
})

const upload=multer({storage})

const Health = require('../models/health');



router.post('/info',upload.single('file'),(req,res,next)=>{
  console.log(req.file);
    let newHealth=new Health({
        age:req.body.age,
        bmi:req.body.bmi,
        height:req.body.height,
        weight:req.body.weight,
        sleepPerDay:req.body.sleepPerDay,
        maritalst:req.body.maritalst,
        medInsurance:req.body.medInsurance,
        bloodgp:req.body.bloodgp,
        userId:req.body.userId,
        // profileImage:req.body.profileImage
    })

    Health.addHealth(newHealth,(err,health)=>{
        if(err){

          res.json({success: false, msg:'Failed to add health Info'});
    } else {
      res.json({success: true, msg:'Health Info Entered'});
    }

    })

})

router.get('/healthDetails',(req,res,next)=>{
   Health.find((err,health)=>{
        if(err){
            throw new Error("Unable to find")
        }
      else{
           res.json({
          success:true,
          health:{
id:health._id,
bmi:health.bmi,
age:health.age,
sleepPerDay:health.sleepPerDay,
height:health.height,
weight:health.weight,
maritalst:health.maritalst,
medInsurance:health.medInsurance,
bloodgp:health.bloodgp,
// profileImage:health.profileImage

          }


        })

      }
    })
})

router.get('/healthDetails/:id',(req,res,next)=>{

  const myId=req.params.id
  console.log(myId);
  Health.findOne({userId:myId},(err,health)=>{
    if(err)
    throw err
    else{
      console.log(health);
      // res.send(health)
             res.json({
          success:true,
          health:{
id:health._id,
bmi:health.bmi,
age:health.age,
sleepPerDay:health.sleepPerDay,
height:health.height,
weight:health.weight,
maritalst:health.maritalst,
medInsurance:health.medInsurance,
bloodgp:health.bloodgp,
// profileImage:health.profileImage


          }


        })

    }
  })
})


module.exports = router;
