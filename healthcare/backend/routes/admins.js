const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const Admin = require('../models/admin');
const config=require('../config/db')

// Register
router.post('/registers', (req, res, next) => {
  let newAdmin = new Admin({
    
    adminId: req.body.adminId,
    username: req.body.username,
    password: req.body.password,
    phone:req.body.phone,
    Address:req.body.Address,
     
      

  });

  Admin.addAdmin(newAdmin, (err, user) => {
    if(err){
      res.json({success: false, msg:'Failed to register user'});
    } else {
      res.json({success: true, msg:'User registered'});
    }
  });
});

// Authenticate
// Authenticate
router.post('/authenticate', (req, res, next) => {
  const adminId = req.body.adminId;
  const password = req.body.password;

  Admin.getAdminByAdminId(adminId, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'Admin not found'});
    }

    Admin.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch){
        const token = jwt.sign({data:user}, config.secret, {
          expiresIn: 604800 // 1 week
        });

        res.json({
          success: true,
          token: 'JWT '+token,
          user: {
            id: user._id,
            
            username: user.username,
            adminId: user.adminId,
            phone:user.phone,
            Address:user.Address,

           

          }
        });
      } else {
        return res.json({success: false, msg: 'Wrong password'});
      }
    });
  });
});

// Profile
router.get('/profiles', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  res.json({user: req.user});
});

module.exports = router;
