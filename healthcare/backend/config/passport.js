var JwtStrategy = require('passport-jwt').Strategy
  const  ExtractJwt = require('passport-jwt').ExtractJwt;

    const User=require('../models/user')
    const config=require('./db')
module.exports=function(passport){
    let opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey = config.secret

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    console.log(jwt_payload.data);
    User.getUserById( jwt_payload.data._id, function(err, user) {
        if (err) {
            return done(err, false);
        }
        if (user) {
            return done(null, user);
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
}));
}
// const connectionUrl="mongodb+srv://Ankush123:Hello123@cluster0.bwjq8.mongodb.net/register?retryWrites=true&w=majority";
