import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserInformation } from '../UserInformation';


@Injectable({
  providedIn: 'root'
})
export class OtpAuthService {

  constructor(private http: HttpClient, private router: Router) { }


  getOTPCode(phoneNumber) {
    return this.http.get<any>(`http://localhost:9001/login?phonenumber=${phoneNumber}&channel=sms`);
  }

  checkCode(phoneNumber, verifyNumber) {
    return this.http.get<any>(`http://localhost:9001/verify?phonenumber=${phoneNumber}&code=${verifyNumber}`);
  }

  logIn() {
    return !!localStorage.getItem('token');  //it will return true or false depending token present in localStorage
  }

  getToken() {
    return localStorage.getItem('token');
  }

  logoutUser() {
    localStorage.removeItem('token');  // It will remove token.
    this.router.navigate(['/otp']);

  }
  sendphoneNumber(phoneNumber) {
    console.log(phoneNumber);
    // var headers=new HttpHeaders();
    // headers.append('Content-Type','application/json');
    return this.http.get<UserInformation>(`http://localhost:9001/PNumber/${phoneNumber}`);
  }

}




