import { TestBed } from '@angular/core/testing';

import { OtpAuthService } from './otp-auth.service';

describe('OtpAuthService', () => {
  let service: OtpAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OtpAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
