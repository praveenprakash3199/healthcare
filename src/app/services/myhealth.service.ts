import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class MyhealthService {
  health:any
  authToken:any
  id:any

  constructor(private http: HttpClient) { }
  addHealth(health) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:9001/healths/info', health, { headers: headers })
      .pipe(map(res => res))
  }

  getHealth() {
    let headers = new HttpHeaders();
    
    
    // headers.append('Authorization', this.authToken);
    // headers.append('Content-Type', 'application/json');
    // headers = headers.append('Authorization', this.authToken);
    headers = headers.append('Content-Type', 'application/json');


    return this.http.get(`http://localhost:9001/healths/healthDetails`, { headers })
      .pipe(map(res => res
      
      ))

      

  }
  getHealthByID(id:any) {
    let headers = new HttpHeaders();

    // headers.append('Authorization', this.authToken);
    // headers.append('Content-Type', 'application/json');
    // headers = headers.append('Authorization', this.authToken);
    headers = headers.append('Content-Type', 'application/json');


    return this.http.get(`http://localhost:9001/healths/healthDetails/${id}`, { headers })
      .pipe(map((res) => res,
    

      ))

    // return this.getHealth().pipe(map(data=>(data as any).find(myHealth=>myHealth.id===id)))



  }
}
