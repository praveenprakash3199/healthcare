import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AdminLoginService } from '../services/admin-login.service';
import { AuthService } from '../services/auth.service';
import { OtpAuthService } from '../services/otp-auth.service';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router,private otpService:OtpAuthService,
        private adminService:AdminLoginService) {

    }

    canActivate() {
        if (this.authService.loggedIn()) {
            return true;
        } 
        else if(this.otpService.logIn()){
            return true
        }
        else if(this.adminService.loggedIn()){
            return false
        }
        
        else {
            this.router.navigate(['/logins']);
            return false;
        }
    }
}
