import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule,Routes} from '@angular/router'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {ValidateService} from './services/validate.service'
import { FlashMessagesModule ,FlashMessagesService} from 'angular2-flash-messages';
// import {AuthService} from 'AuthService';
import { AuthService} from './services/auth.service'
import {HttpClientModule} from '@angular/common/http'
import {AuthGuard} from '../app/guards/auth.guard';
import { HealthComponent } from './components/health/health.component'
import { MyhealthService } from './services/myhealth.service';
import { TempComponent } from './components/temp/temp.component';
import { OTPComponent } from './components/otp/otp.component';
import { OtpAuthService } from './services/otp-auth.service';
import { MedicineComponent } from './components/medicine/medicine.component';
import { UserInformationComponent } from './components/user-information/user-information.component';
import { UserInformationService } from './services/user-information.service';
import { MedicineCovid19Component } from './components/medicine-covid19/medicine-covid19.component';
import { MedicineCoughComponent } from './components/medicine-cough/medicine-cough.component';
import { MedicineFeverComponent } from './components/medicine-fever/medicine-fever.component';
import { MedicineMalariaComponent } from './components/medicine-malaria/medicine-malaria.component';
import { AdminComponent } from './components/admin/admin.component';
import { DiagnosticsComponent } from './components/diagnostics/diagnostics.component';
import { DiabeticsComponent } from './components/diabetics/diabetics.component';
import { XrayComponent } from './components/xray/xray.component';
import { MriComponent } from './components/mri/mri.component';
import { CovidComponent } from './components/covid/covid.component';
import { CtComponent } from './components/ct/ct.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { AdminDiagnosticsComponent } from './components/admin-diagnostics/admin-diagnostics.component';
import { ListAppointmentsComponent } from './components/list-appointments/list-appointments.component';
import { DiagnosticsAppointmentsComponent } from './components/diagnostics-appointments/diagnostics-appointments.component';
import { ListComponent } from './components/list/list.component';
import { DoctorComponent } from './components/doctor/doctor.component';
import { DoctorsAppointmentComponent } from './components/doctors-appointment/doctors-appointment.component';
import { BookComponent } from './components/book/book.component';
import { AdminDoctorComponent } from './components/admin-doctor/admin-doctor.component';
import { BmicalculatorComponent } from './components/bmicalculator/bmicalculator.component';
import { CaloriemeterComponent } from './components/caloriemeter/caloriemeter.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { AdminProfileComponent } from './components/admin-profile/admin-profile.component';
import { ProfileOtpComponent } from './components/profile-otp/profile-otp.component';
import { HealthOtpComponent } from './components/health-otp/health-otp.component';



const appRoutes:Routes=[
{path:'',component:HomeComponent},
{path:'register',component:RegisterComponent},
{path:'dashboard',component:DashboardComponent,canActivate:[AuthGuard]},
{path:'profile',component:ProfileComponent,canActivate:[AuthGuard]},
{path:'profileOtp',component:ProfileOtpComponent,canActivate:[AuthGuard]},
{path:'logins',component:LoginComponent},

{path:'admin-login',component:AdminLoginComponent},
  { path: 'admin/admin-profile', component: AdminProfileComponent},




{path:'profile/health/:id',component:HealthComponent},
{path:'profileOtp/healthOtp/:id',component:HealthOtpComponent},
{path:'temp',component:TempComponent},
  { path: 'otp', component: OTPComponent },
  { path: 'userInfo', component: UserInformationComponent },
  { path: 'medicine', component: MedicineComponent },
  { path: 'medicineCovid19', component: MedicineCovid19Component, canActivate: [AuthGuard] },
  { path: 'medicineFever', component: MedicineFeverComponent, canActivate: [AuthGuard] },
  { path: 'medicineCough', component: MedicineCoughComponent, canActivate: [AuthGuard] },
  { path: 'medicineMalaria', component: MedicineMalariaComponent, canActivate: [AuthGuard] },
  { path: 'diagnostics', component: DiagnosticsComponent },
  { path:'diagnostics/diabetics',component:DiabeticsComponent},
  { path: 'diagnostics/xray', component: XrayComponent },
  { path: 'diagnostics/mri', component: MriComponent },
  { path: 'diagnostics/covid', component: CovidComponent },
  { path: 'diagnostics/ct', component: CtComponent },
  { path: 'admin', component: AdminDashboardComponent },
  { path: 'admin/admin-diagnostics', component:AdminDiagnosticsComponent  },
  { path: 'admin/admin-diagnostics/list-appointments', component: ListAppointmentsComponent },
  { path: 'admin/admin-diagnostics/diagnostics-appointments', component: DiagnosticsAppointmentsComponent },


  //user_doctor
  {path:'doctor',component:DoctorComponent},
  {path:'doctor/book',component:BookComponent},


  //admin_doctor
  {path:'admin-doctor',component:AdminDoctorComponent},
  {path:'list',component:ListComponent},
  {path:'doctors-appointment',component:DoctorsAppointmentComponent},


  //sirisha
  {path:'bmi',component:BmicalculatorComponent},
  {path:'calorie',component:CaloriemeterComponent},






  { path: "", redirectTo: "dashboard", pathMatch: "full"},

  { path: "**", redirectTo: "dashboard" },








]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    ProfileComponent,
    DashboardComponent,
    HomeComponent,
    RegisterComponent,
    HealthComponent,
    TempComponent,
    OTPComponent,
    MedicineComponent,
    UserInformationComponent,
    MedicineCovid19Component,
    MedicineCoughComponent,
    MedicineFeverComponent,
    MedicineMalariaComponent,
    AdminComponent,
    DiagnosticsComponent,
    DiabeticsComponent,
    XrayComponent,
    MriComponent,
    CovidComponent,
    CtComponent,
    AdminDashboardComponent,
    AdminDiagnosticsComponent,
    ListAppointmentsComponent,
    DiagnosticsAppointmentsComponent,
    ListComponent,
    DoctorComponent,
    DoctorsAppointmentComponent,
    BookComponent,
    AdminDoctorComponent,
    BmicalculatorComponent,
    CaloriemeterComponent,
    AdminLoginComponent,
    AdminProfileComponent,
    ProfileOtpComponent,
    HealthOtpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    FlashMessagesModule,
    HttpClientModule,
   ReactiveFormsModule

  ],
  providers: [ValidateService,FlashMessagesService,AuthService,AuthGuard,MyhealthService,OtpAuthService,
  UserInformationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
