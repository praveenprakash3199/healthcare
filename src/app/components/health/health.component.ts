import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service'
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service'
import { ActivatedRoute, Params, Router } from '@angular/router';

import{MyhealthService} from '../../services/myhealth.service'
@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.css']
})
export class HealthComponent implements OnInit {
bmi:Number
height:Number
weight:Number
bloodgp:String
maritalst:String
medInsurance:Boolean
sleepPerDay:Number
age:Number
userId:any
profileImage:any
fileToUpload:any
url
file

  constructor(private validateService: ValidateService
    , private flashMessage: FlashMessagesService,
    private healthService:MyhealthService,
    private router: Router,
    private route:ActivatedRoute
    ) { }

  ngOnInit(): void {
   this.route.paramMap.subscribe(params=>{
     this.userId=params.get('id')
     console.log(this.userId);
    //  this.profileImage=params.get('profileImage')
    //  console.log('my Image',this.profileImage);

   })


  }
  onSubmitMyHelathInfo(){
    const myHealthInfo={
      bmi:this.bmi,
      height:this.height,
      weight:this.weight,
      bloodgp:this.bloodgp,
      maritalst:this.maritalst,
      medInsurance:this.medInsurance,
      sleepPerDay:this.sleepPerDay,
      age:this.age,
      userId:this.userId,
      // profileImage:this.profileImage

    }
    console.log(myHealthInfo);

    if(!this.validateService.validateHealthInfo(myHealthInfo)){
      this.flashMessage.show('Please fill in all fields', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }
    this.healthService.addHealth(myHealthInfo).subscribe(data => {
      if ((data as any).success) {
        this.flashMessage.show('You Have SuccessfullyEnteredData', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/profile']);
      } else {
        this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });
        this.router.navigate(['/health']);
      }
    });




  }
  handleFileInput(event:any) {
    // if (event.target.files && event.target.files[0]) {
    //   var reader = new FileReader();

    //   reader.onload = (event: any) => {
    //     this.url = (<FileReader>event.target).result;
    //   }

    //   reader.readAsDataURL(event.target.files[0]);
    // }
    this.file = event.target.files[0]
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event:any) => {
      this.url = (event.target).result;
      console.log(this.file.name);
      // this.fileToUpload=this.file.name


    }

  }

}
