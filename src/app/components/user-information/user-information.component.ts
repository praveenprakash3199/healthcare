import { UserInformation } from './../../UserInformation';
import { UserInformationService } from '../../services/user-information.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['./user-information.component.css']
})
export class UserInformationComponent implements OnInit {

  UserForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private userInfo: UserInformationService) {
    this.createUserForm();
  }

  ngOnInit(): void {
  }

  createUserForm() {
    this.UserForm = this.fb.group({
      FullName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phoneNumber: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required])
    })
  }

  FullName: string;
  email: string;
  phoneNumber: string;
  address: string;
  userInformation:UserInformation
  info
  infoArray:any[]=[]
  addInformation() {
    this.userInformation = {
      FullName: this.FullName,
      email: this.email,
      phoneNumber: this.phoneNumber,
      address: this.address
    }
    console.log(this.userInformation);

    this.userInfo.addInfo(this.userInformation).subscribe(data => {
      console.log("from user-information component",data);
      // this.infoArray.push(data)
      window.localStorage.setItem('userInformation',JSON.stringify(data))
      this.info=window.localStorage.getItem('userInformation')
      console.log("from user-information component",this.info)
      this.router.navigate(['/dashboard']);
    });





  }
}
