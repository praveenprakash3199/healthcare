import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DoctorsService} from '../../services/doctors.service';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  first_name= new FormControl('',[Validators.required])
  last_name= new FormControl('',[Validators.required])
  age= new FormControl('',[Validators.required,])

  gender = new FormControl('',[Validators.required])

  test=new FormControl('',[Validators.required])
  registerForm: FormGroup
  genders=['male','female']

  phoneNumber= new FormControl('', [Validators.required, Validators.pattern(`^\\d{10}$`)])

  date= new FormControl('',[Validators.required])
  time= new FormControl('',[Validators.required]);

  detailsArray:any[]=[];
  details:any;


    constructor(private fb : FormBuilder, private service : DoctorsService) {
      this.registerForm= this.fb.group({
        first_name: this.first_name,
        last_name: this.last_name,
        age   : this.age,
       gender: this.gender,
       test: this.test,
       date: this.date,
      time:this.time,
      phoneNumber :this.phoneNumber,



      })
    }
    onRegister(){
      console.log(this.registerForm);
      alert('Submitted');

    }

    ngOnInit(): void {
      this.service.getData().subscribe(userdata=> this.detailsArray=userdata)
    }
    addContact(){

alert('Submitted Successfully');
      const newData={
        first_name: this.registerForm.value.first_name,
        last_name: this.registerForm.value.last_name,
        age: this.registerForm.value.age,
        gender: this.registerForm.value.gender,
        test: this.registerForm.value.test,
        date: this.registerForm.value.date,
        time:this.registerForm.value.time,
        phoneNumber: this.registerForm.value.phoneNumber
      }

      this.service.addContact(newData).subscribe(contact=>{
        this.detailsArray.push(contact)
      })
    }


}


