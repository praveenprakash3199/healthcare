import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DiagnosticsService } from '../../services/diagnostics.service';
@Component({
  selector: 'app-xray',
  templateUrl: './xray.component.html',
  styleUrls: ['./xray.component.css']
})
export class XrayComponent implements OnInit {
  name = new FormControl('', [Validators.required])

  age = new FormControl('', [Validators.required])

  gender = new FormControl('', [Validators.required])

  test: string = 'X-Ray'
  registerForm: FormGroup
  genders = ['male', 'female']

  mobileNumber = new FormControl('', [Validators.required, Validators.pattern('^\\d{10}$')])

  date = new FormControl('', [Validators.required])
  time = new FormControl('', [Validators.required]);

  detailsArray: any[] = [];
  details: any;


  constructor(private fb: FormBuilder, private service: DiagnosticsService) {
    this.registerForm = this.fb.group({
      name: this.name,
      age: this.age,
      gender: this.gender,
      test: this.test,
      date: this.date,
      time: this.time,
      mobileNumber: this.mobileNumber,



    })
  }
  onRegister() {
    console.log(this.registerForm);
    alert('Submitted');

  }

  ngOnInit(): void {
    this.service.getData().subscribe(userdata => this.detailsArray = userdata)
  }
  addContact() {

    alert('Submitted Successfully');
    const newData = {
      name: this.registerForm.value.name,
      age: this.registerForm.value.age,
      gender: this.registerForm.value.gender,
      test: this.registerForm.value.test,
      date: this.registerForm.value.date,
      time: this.registerForm.value.time,
      mobileNumber: this.registerForm.value.mobileNumber
    }

    this.service.addContact(newData).subscribe(contact => {
      this.detailsArray.push(contact)
    })
  }


}
