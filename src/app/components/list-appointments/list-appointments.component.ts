import { Component, OnInit } from '@angular/core';

import { DiagnosticsService } from '../../services/diagnostics.service';

@Component({
  selector: 'app-list-appointments',
  templateUrl: './list-appointments.component.html',
  styleUrls: ['./list-appointments.component.css']
})
export class ListAppointmentsComponent implements OnInit {
  public sortBy: any = ''
  public results: any[] = []
  constructor(private service: DiagnosticsService) { }

  ngOnInit(): void {
  }
  sortResults(sort: any) {

    this.results = []

    this.service.getTest(sort).subscribe(res => {
      console.log(res);
      return this.results = res;    //find
      //return this.results.push(res)   findOne

    })
  }

}
