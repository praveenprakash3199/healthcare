import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicineCoughComponent } from './medicine-cough.component';

describe('MedicineCoughComponent', () => {
  let component: MedicineCoughComponent;
  let fixture: ComponentFixture<MedicineCoughComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicineCoughComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicineCoughComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
