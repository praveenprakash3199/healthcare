import { Component, OnInit } from '@angular/core';
import {DoctorsService} from '../../services/doctors.service';
import {Observable} from 'rxjs';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public sortBy:any=''
  public results:any[]=[]
    constructor(private service : DoctorsService) { }

    ngOnInit(): void {
    }
    sortResults(sort:any){

      this.results=[]

      this.service.getTest(sort).subscribe(res=>{
        console.log(res);
        return this.results=res;
      })
    }


}
