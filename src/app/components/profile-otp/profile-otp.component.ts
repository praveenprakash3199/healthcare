import { MyhealthService } from './../../services/myhealth.service';
import { UserInformation } from './../../UserInformation';
import { OtpAuthService } from './../../services/otp-auth.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-otp',
  templateUrl: './profile-otp.component.html',
  styleUrls: ['./profile-otp.component.css']
})
export class ProfileOtpComponent implements OnInit {

  @Input()  information:any;
  @Input() myHealth
  user: any
  health:any
  addMyHealthData:Object[]=[]
  id:Number
  healths:any
  userId:Number
  otpUser:any
  myphoneNumber:any
  constructor(private otpService:OtpAuthService,private myhealthService:MyhealthService) { }

  informations:UserInformation[]=[];
  SingleInfo
  info:any[]=[];
  ngOnInit(): void {

    this.SingleInfo=window.localStorage.getItem('userInformation') ? JSON.parse(localStorage.getItem('userInformation')):[];
    // this.SingleInfo=window.localStorage.getItem('userInformation')
      console.log("from profile-otp component",this.SingleInfo)
    this.info.push(this.SingleInfo)
    // console.log(this.SingleInfo);
    // console.log(this.info);

    this.myhealthService.getHealth().subscribe(data => {
      // console.log(data);
      // this.addMyHealthData.push(data)

      this.health = (data as any)
      // console.log((data as any).health);
      // console.log('my health', this.addMyHealthData);

      console.log("Single info ",this.SingleInfo._id);

      this.myhealthService.getHealthByID(this.SingleInfo._id).subscribe(data => {
        this.healths = (data as any).health
        // console.log(this.healths);

      })

    })

  }

}
