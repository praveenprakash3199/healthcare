import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { OtpAuthService } from 'src/app/services/otp-auth.service';
import { AdminLoginService } from 'src/app/services/admin-login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    public otpService:OtpAuthService
    ,public adminService:AdminLoginService) { }

  ngOnInit(): void {
    
  }

  //logout functionality
  onLogoutClick() {
    this.authService.logout();
    this.flashMessage.show('You are logged out', {
      cssClass: 'alert-success',
      timeout: 500 //timig duration
    });
    this.router.navigate(['/']); //take to login page
    return false;
  }

}
