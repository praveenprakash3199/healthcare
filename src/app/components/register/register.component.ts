import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service'
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService} from '../../services/auth.service'
import { Router } from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 
  username: String;
  email: String;
  password: String;
  Address:String;
  phone: Number
  myId:Number
  cpassword: String;


  constructor(private validateService:ValidateService,private flashMessage:FlashMessagesService
    , private authService: AuthService
    , private router: Router
    ) { }

  ngOnInit(): void {
  }
  onRegisterSubmit(){
    const user = {

      email: this.email,
      username: this.username,
      password: this.password,
      phone:this.phone,
      Address:this.Address,
      myId:this.myId,
      cpassword: this.cpassword,

     
    }
    console.log('user registerd ',user);
    

    // Required Fields
    if (!this.validateService.validateRegister(user)) {
      this.flashMessage.show('Please fill in all fields', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    // Validate Email
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessage.show('Please use a valid email', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }
if(!this.validateService.validatePassword(user.password)){
  this.flashMessage.show('Please enter password more than 6 characters',{cssClass:'alert-danger',timeout:3000})
  return false
}
if(!this.validateService.validateConfirmPassword(user.password,user.cpassword)){
  this.flashMessage.show('Password doesnot match', { cssClass: 'alert-danger', timeout: 3000 })
  return false
}

// if(!this.validateService.validatePhone(user.phone)){
//   this.flashMessage.show('Phone Number Incorrect', { cssClass: 'alert-danger', timeout: 3000 });
//   return false;
// }


    
    this.authService.registerUser(user).subscribe(data => {
      if ((data as any).success) {
        this.flashMessage.show('You are now registered and can log in', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/logins']);
      } else {
        this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });
        this.router.navigate(['/register']);
      }
    });


  }

}
