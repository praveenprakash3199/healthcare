import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnosticsAppointmentsComponent } from './diagnostics-appointments.component';

describe('DiagnosticsAppointmentsComponent', () => {
  let component: DiagnosticsAppointmentsComponent;
  let fixture: ComponentFixture<DiagnosticsAppointmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiagnosticsAppointmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosticsAppointmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
