import { OtpAuthService } from '../../services/otp-auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-medicine',
  templateUrl: './medicine.component.html',
  styleUrls: ['./medicine.component.css']
})
export class MedicineComponent implements OnInit {
  constructor(private router: Router, public otpService: OtpAuthService) { }

  ngOnInit(): void { }

  medicineCovid19() {
    this.router.navigate(['/medicineCovid19'])
  }

  medicineCough() {
    this.router.navigate(['medicineCough'])
  }

  medicineFever() {
    this.router.navigate(['medicineFever'])
  }

  medicineMalaria() {
    this.router.navigate(['medicineMalaria'])
  }
}
