export class UserInformation {
    _id?: string;
    FullName: string;
    email: string;
    phoneNumber: string;
    address: string;
}
