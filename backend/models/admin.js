const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/db');

// User Schema
const AdminSchema = mongoose.Schema({

  adminId: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  phone:{
    type:Number,
    required:true
  },
  Address:{
    type:String,
    required:true
  }
    
});

const Admin = module.exports = mongoose.model('Admins', AdminSchema);

module.exports.getAdminById = function(id, callback){
  Admin.findById(id, callback);
}

module.exports.getAdminByAdminId = function(adminId, callback){
  const query = {adminId: adminId}
  Admin.findOne(query, callback);

}

module.exports.addAdmin = function(newAdmin, callback){
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newAdmin.password, salt, (err, hash) => {
      if(err) throw err;
      newAdmin.password = hash;
      newAdmin.save(callback);

});
  });
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  });
}
