const mongoose = require("mongoose");
const bmiSchema=  mongoose.Schema({
    name : {
        type : String,
        required : true,
    },
    height :{
       type:String,
       required : true,
   },
   weight :{
     type: String,
     required:true,
   },
  
})

const bmi = mongoose.model("bmi",bmiSchema);
module.exports={bmi};




