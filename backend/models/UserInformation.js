const mongoose=require("mongoose");
const userInfoSchema=mongoose.Schema({
  FullName:{
        type:String,
        required:true
    },
  email:{
    type:String,
    trim: true,
    lowercase: true,
    unique: true,
  },
  phoneNumber:{
    type: String,
    required: true
  },
  address:{
      type:String,
      required:true
  }})
const UserInfoModel=mongoose.model("UserInfo",userInfoSchema);
module.exports={ UserInfoModel };