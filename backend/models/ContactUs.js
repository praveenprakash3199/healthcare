const mongoose=require("mongoose");
const ContactInfoSchema=mongoose.Schema({
  name:{
        type:String,
        required:true
    },
  email:{
    type:String,
    trim: true,
    lowercase: true,
    unique: true,
  },
  phone:{
    type: String,
    required: true
  },
  message:{
      type:String,
      required:true
  }})
const ContactInfoModel=mongoose.model("ContactInfo",ContactInfoSchema);
module.exports={ ContactInfoModel };
