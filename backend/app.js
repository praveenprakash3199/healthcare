const express=require('express')
const path=require('path')
const bodyParser=require('body-parser')
const cors=require('cors')
const mongoose=require('mongoose')
mongoose.set('useCreateIndex', true);
const {ContactInfoModel}=require("./models/ContactUs")
//ayyappa
const { Doctor } = require("./models/models");
const passport=require('passport')
const config=require('./config/db')
const configs=require('./configs')
const client=require('twilio')(configs.accountSID,configs.authToken);
const {UserInfoModel}=require('./models/UserInformation')
const {TodoModel} = require('./models/middleware');
const methodOverride = require('method-override');

//sirisha
const {calorie}=require("./models/user.model");
const {bmi}=require("./models/user1.model");


const admins=require('./routes/admins')

const jwt = require('jsonwebtoken');

mongoose.connect(config.database,{useNewUrlParser:true,useUnifiedTopology:true}).then(
    (res)=>{
        console.log('database connected');
    }
).catch((err)=>{
    console.log('database not connected');
})
const app=express()
app.use(cors());
const users=require('./routes/users')

const healths=require('./routes/healths')
//port number
const port=9001
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/login',(req,res)=>{

    client.verify.services(configs.serviceID).verifications.create({
        to:`+${req.query.phonenumber}`,
        channel:req.query.channel,
    }).then((data) =>{
        res.status(200).send(data);
    })
})

app.get('/verify',(req,res)=>{

    client.verify.services(configs.serviceID).verificationChecks.create({
        to:`+${req.query.phonenumber}`,
        code:req.query.code,
    }).then((data) =>{
        console.log(data);
        let payload={subject: data._id};
        let token=jwt.sign(payload,'secretKey');
        res.status(200).send({token});
    }).catch(error=>res.status(401).send("Unauthorized OTP login"));
})


app.post('/UserInfo',(req,res)=>
{
    if(req.body)
    {
        console.log(req.body);
        const UserItem=new UserInfoModel(req.body);
        UserItem.save().then(response =>
            {
                return res.send(response);
            }).catch(err =>
                {
                 res.send("Validation Failed")});
                }
    //return res.send("POST API");
})


app.get('/PNumber/:phoneNumber',(req,res)=>{
    if(req.params)
    {
        const phoneNumber=req.params.phoneNumber;
        console.log(phoneNumber);
        UserInfoModel.findOne({phoneNumber}).then((data)=>{
            console.log(data);
            res.send(data)
            // res.status(200).send(data,token);
        }).catch(err=>res.status(401).send("Unauthorized user"));
    }
})





app.get("/diagnostics", (req,res)=>{
    TodoModel.find((err,docs)=>{
        if(err) console.log(err);
        return res.send(docs);
    })
})


app.use(methodOverride("_method",{
  methods:['POST',"GET"],
}));


app.get("/dynamic",(req,res)=>{
 TodoModel.find({}, function(err,diagnostics){
     res.render('index',{
         diagnostics_appointments : diagnostics
     })
 })
})



app.post("/diagnostics",(req,res)=>{
   if(req.body){
      //  console.log(req.body);
       const todoItem = new TodoModel(req.body);
       todoItem.save()
       .then(response=>{
            return res.send(response);
       })
       .catch(err=>console.log(err));
   }
})

app.delete("/diagnostics/:name",(req,res)=>{
  if(req.params){
  const name =req.params.name;
   TodoModel.deleteOne({name})
      .then(response=>{
          return res.send(response);
      }).catch(err => console.log(err))
  }
})

app.get("/diagnostics/:test",(req,res)=>{
  if(req.params){
    const test = req.params.test;
    TodoModel.find({test})
    .then(response=>{
      return res.send(response);
    })
    .catch(err=> console.log(err))
  }

})


app.post('/contactUs',(req,res)=>
{
    if(req.body)
    {
        console.log(req.body);
        const contactItem=new ContactInfoModel(req.body);
        contactItem.save().then(response =>
            {
                return res.send(response);
            }).catch(err =>
                {
                 res.send("Validation Failed")});
                }
    //return res.send("POST API");
})




//ayyappa

app.get("/Doctors", (req, res) => {
  Doctor.find().then(docs => res.send(docs))
})
app.use(methodOverride("_method",{
  methods:['POST',"GET"],
}));
app.get("/dynamic",(req,res)=>{
  Doctor.find({}, function(err,diagnostics){
      res.render('index',{
          doctors_appointments : doctors
      })
  })
})



app.post("/Doctors", (req, res) => {
  if (req.body) {
      console.log(req.body);
      const doctor = new Doctor(req.body);
      doctor.save()
          .then(response => {
              return res.send(response);
          }).catch(err => res.send(err.message));
  }
})




app.delete("/Doctors/:phoneNumber", (req, res) => {
  Doctor.deleteOne({phoneNumber:req.params.phoneNumber}, function(err,result){
      if(err){
          res.json(err);
      }else{
          res.json(result);
      }
  })

});
app.get("/Doctors/:test",(req,res)=>{
  if(req.params){
    const test = req.params.test;
    Doctor.find({test})
    .then(response=>{
      return res.send(response);
    })
    .catch(err=> console.log(err))
  }

});

//sirisha
app.get("/calorie",(req,res)=>{
  calorie.find().then((response)=>{
       return res.send(response)
  }).catch((error)=>{
      console.log(error);
  })
})
app.get("/calorie/:id",(req,res)=>{
  const id=req.params.id;
  calorie.findById(id)
  .then((docs)=>{
      return res.send(docs)
  })
  .catch(err=> console.log(err));

})
app.post("/calorie",(req,res)=>{
  console.log(req.body);
  if(req.body){
      const newUser=new calorie(req.body);
      newUser.save().then((response)=> res.send(response))
      .catch(err=> console.log(err));
  }else{
      res.send("insufficient data to post");
  }
})

app.delete("/calorie/:id",(req,res)=>{
  const id=req.params.id;
  calorie.findByIdAndDelete(id)
  .then(()=>res.send("user data is deleted"))
  .catch(()=> res.send("could not find data with given id to delete"))

})
app.patch("/calorie/:id",(req,res)=>{
  const id=req.params.id;
  calorie.findByIdAndUpdate(id,update={

   }).then((response)=> res.send(response))
   .then(()=> res.send("data is updated"))
   .catch((err)=> console.log(err))
})

// bmi
app.get("/bmi",(req,res)=>{
  bmi.find().then((response)=>{
       return res.send(response)
  }).catch((error)=>{
      console.log(error);
  })
})
app.get("/bmi/:id",(req,res)=>{
  const id=req.params.id;
  bmi.findById(id)
  .then((docs)=>{
      return res.send(docs)
  })
  .catch(err=> console.log(err));

})
app.post("/bmi",(req,res)=>{
  console.log(req.body);
  if(req.body){
      const newUser=new bmi(req.body);
      newUser.save().then((response)=> res.send(response))
      .catch(err=> console.log(err));
  }else{
      res.send("insufficient data to post");
  }
})

app.delete("/bmi/:id",(req,res)=>{
  const id=req.params.id;
  bmi.findByIdAndDelete(id)
  .then(()=>res.send("user data is deleted"))
  .catch(()=> res.send("could not find data with given id to delete"))

})
app.patch("/bmi/:id",(req,res)=>{
  const id=req.params.id;
  bmi.findByIdAndUpdate(id,update={

   }).then((response)=> res.send(response))
   .then(()=> res.send("data is updated"))
   .catch((err)=> console.log(err))
})















app.use(bodyParser.urlencoded({ extended: true }));

//cors middleware


//bodyparser middleware
// app.use(bodyParser.json())


// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads',express.static('uploads'))

// app.use('/users',users)
app.use('/users',users)
app.use('/healths',healths)
app.use('/admins',admins)



//passport mw
app.use(passport.initialize())
app.use(passport.session())
require('./config/passport')(passport)

//Index route
app.get('/',(req,res)=>{
    res.send('<h1>Invalid EndPoint</h1>')
})

app.listen(port,()=>{
    console.log('Server up and running on '+port);
})


